---
title: "Using VR to explain Kubernetes"
date: 2024-10-05T09:00:00+12:00
slug: 'Using VR to explain Kubernetes'
hero: /images/posts/vr-hero.png
draft: false
description: "Using VR to explain Kubernetes"
tags: ['cloud native','cloud','cncf','docker','kubernetes','devops']
categories: ['conference','tech', 'virtual reality']
series: ['cncf','devops','cloud','cloud native', 'vr/ar']

---
In 2019, I gave a presentation at the [CNCF Meetup](https://youtu.be/vAtnoyrN86g?si=tB9Gb9R3gILBO2wL) to explain how chaos engineering can help us verify that our applications are fault-tolerant. To do this, I used Minecraft to visualize Pods being deleted by Chaos-Kube and recreated by Kubernetes.

Today, five years later, I can show you a platform we developed in Unity to visualize Kubernetes concepts through a Virtual Reality experience.

This experience focuses on explaining basic concepts, such as the definition of a POD, its resources, and how processes like "rolling upgrades" work.

We’re working on additional scenarios to showcase how tools like Falco, eBPF, Cert-Manager, External-DNS, and other applications operate on Kubernetes.

Here's a small demonstration of the proof of concept in action.

Hope you enjoy it! :)

{{< youtube LJi4S0wzdaw >}}