---
title: "Usando Realidad Virtual para explicar conceptos de la nube"
date: 2024-10-05T09:00:00+12:00
slug: 'Realidad Virtual y Kubernetes'
hero: /images/posts/vr-hero.png
draft: false
description: "Usando Realidad Virtual para explicar conceptos de la nube"
tags: ['cloud native','cloud','cncf','docker','kubernetes','devops']
categories: ['charlas','tecnologia', 'realidad-virtual']
series: ['cncf','devops','cloud','cloud native', 'vr/ar']

---
En 2019, realicé una presentación en la meetup de
[CNCF Meetup](https://youtu.be/vAtnoyrN86g?si=tB9Gb9R3gILBO2wL)
para explicar cómo la ingeniería del caos puede ayudarnos a comprobar que nuestras aplicaciones son tolerantes a fallos. Para esto, usé Minecraft de manera de poder visualizar los Pods siendo eliminados por *Chaos-Kube* y recreados por *Kubernetes*

Hoy, cinco años después, les puedo mostrar una plataforma que desarrollamos en Unity para visualizar conceptos de Kubernetes mediante una experiencia de Realidad Virtual.

Esta experiencia se centra en explicar conceptos básicos, tales como la definición de un `POD`, sus recursos, y cómo funcionan procesos como el "rolling upgrade". 

Seguimos trabajando en escenarios adicionales para mostrar el funcionamiento de herramientas como `Falco`, `eBPF`, `Cert-Manager`, `External-DNS` y otras aplicaciones que operan sobre `Kubernetes`.

Aquí les dejo una pequeña demostración de la prueba de concepto en acción.

Espero que les guste :)

{{< youtube DW8Y_9ChjUE >}}